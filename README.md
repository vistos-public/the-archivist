# The Archivist

A Node-based GPT-3.5-powered chatbot, using the OpenAI API.

The long term goal is to support alternative models, prioritizing those who understand that calling yourself "Open" should be more than a marketing ploy.

## Environment Variables

To deploy The Archivist, you must set some environment variables on your Node instance.

- `APP_ID`: the Discord Application ID
- `PUBLIC_KEY`: The Discord Application's Public Key
- `DISCORD_TOKEN`: The Discord Applications token, from the "Bot" menu
- `OPENAI_KEY`: An OpenAI API key
- `OPENAI_GPT`: The chosen OpenAI GPT model (eg `gpt-3.5-turbo-0301`, `gpt-3.5-turbo`)

For more information an creating a Discord Application, visit the [Discord Developer Portal](https://discord.com/developers/applications).

To sign up for an OpenAI API key, visit the [OpenAI Platform](https://platform.openai.com).

If you're interested in contributing to the project, [contact z3c0](mailto:z3c0@vistos.tech).

## Commands

### `/summon`

Summon The Archivist to respond to the current channel. It will ingest all messages tagged with `@The Archivist`, and its own responses.

### `/inject`

Injects all messages tagged with `@The Archivist` as contextual prompts. These will be included with every call of `/summon`.

To best utilize the `/inject` command, create a separate channel solely for providing context to `The Archivist`.

It is ideal to begin prompts with "The Archivist does x", instead of "you do x". This is because getting ChatGPT to identify as anything other than "an AI Language Model" is tricky. However, convincing it to role-play as an entity and subsequently defining boundaries around that entity tends to be a more effective route for prompt injection.

Note: *currently, this command will need to be re-run upon updates*

### `/reinforce`

Injects the current conversation as a method of reinforcing good responses. Anytime The Archivist exhibits the desired personality, use this command to reinforce future exchanges.

## Known Issues

- Regression to ChatGPT's bland personality can become "locked in" if bad responses are not deleted. This is because the bad messages are being fed into the context with each call of `/summon`. To keep your bot from replying with boring responses (eg "As an AI Language model..."), delete these responses and add corrective injections to your injection channel. A future goal is to use message reactions as a way to flag bad responses, to enable excluding them from the context.

- Occasionally, if unprompted with any question, "The Archivist" will hallucinate a question, such as *z3c0 says "What is..."*. This is due to `"${author} says"` being appended to the front of each prompt by a chat member. This is to assist The Archivist in maintaining a discussion with multiple people. This can be diminished by injecting additional prompts.

## A Fair Warning

Do not use ChatGPT (or any LLMs) for operationally-sensitive decision-making. LLMs are not rule-extraction models, and all apparently logical behavior is caused by increasing the odds towards that outcome with the right context. This does not eradicate unpredictable behavior, only diminishes it. Even if it were correct 99% of the time, that 1 out of 100 can be enough to undercut the success of the other 99. For that reason, keep LLMs angled towards novel uses (discussion, light trivia, etc), and never assume factual outputs - only probable ones.
