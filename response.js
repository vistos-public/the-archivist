
import { InteractionResponseType } from 'discord-interactions';


class Response {
    type = null;
    data = { content: null };
}

export class InjectionNeededResponse extends Response {
    type = InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE;
    data = { content: 'Greetings. I have recently been updated and need to have prompts injected.' };
}

export class DeferredResponse extends Response {
    type = InteractionResponseType.DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE;
}