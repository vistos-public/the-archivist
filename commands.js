import 'dotenv/config';
import fetch from 'node-fetch';


const COMMAND_ARCHIVIST_TEST = {
	name: 'test-archivist',
	description: 'A command for testing The Archivist',
	type: 1
};

const COMMAND_ARCHIVIST_SUMMON = {
	name: 'summon',
	description: 'Summon the Archivist to add to the current discussion',
	type: 1
};

const COMMAND_ARCHIVIST_SUMMON_NO_CTX = {
	name: 'summon-no-ctx',
	description: 'Summon the Archivist to add to the current discussion, ignoring the current conversation',
	type: 1
};

const COMMAND_ARCHIVIST_SUMMON_NO_INJ = {
	name: 'summon-no-inj',
	description: 'Summon the Archivist to add to the current discussion, foregoing prompt injection',
	type: 1
};

const COMMAND_ARCHIVIST_INJECT = {
	name: 'inject',
	description: 'Inject the prompts of the current thread into the model',
	type: 1
};

const COMMAND_ARCHIVIST_REINFORCE = {
	name: 'reinforce',
	description: 'Inject the prompts of the current thread into the model',
	type: 1
};

const ALL_COMMANDS = [
	COMMAND_ARCHIVIST_TEST,
	COMMAND_ARCHIVIST_SUMMON,
	COMMAND_ARCHIVIST_SUMMON_NO_CTX,
	COMMAND_ARCHIVIST_SUMMON_NO_INJ,
	COMMAND_ARCHIVIST_INJECT,
	COMMAND_ARCHIVIST_REINFORCE
];


async function DiscordRequest(endpoint, options) {
	// append endpoint to root API URL
	const url = 'https://discord.com/api/v10/' + endpoint;
	// Stringify payloads
	if (options.body) options.body = JSON.stringify(options.body);
	// Use node-fetch to make requests
	const res = await fetch(url, {
		headers: {
			'Authorization': `Bot ${process.env.DISCORD_TOKEN}`,
			'Content-Type': 'application/json; charset=UTF-8',
			'User-Agent': 'DiscordBot (https://github.com/discord/discord-example-app, 1.0.0)',
		},
		...options
	});
	// throw API errors
	if (!res.ok) {
		const data = await res.json();
		console.log(res.status);
		throw new Error(JSON.stringify(data));
	}
	// return original response
	return res;
}


async function InstallGlobalCommands(appId, commands) {
	// API endpoint to overwrite global commands
	const endpoint = `applications/${appId}/commands`;

	try {
		// This is calling the bulk overwrite endpoint: https://discord.com/developers/docs/interactions/application-commands#bulk-overwrite-global-application-commands
		await DiscordRequest(endpoint, { method: 'PUT', body: commands });
	} catch (err) {
		console.error(err);
	}
}

InstallGlobalCommands(process.env.APP_ID, ALL_COMMANDS);
