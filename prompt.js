
import fs from 'fs';


class Prompt {
	role = null;
	content = null;

	constructor(role, content) {
		this.role = role;
		this.content = content;
	}
}

export const defaultPrompts = JSON.parse(fs.readFileSync('default-prompts.json', 'utf-8'))
	.prompts.map(prompt => new Prompt(prompt.role, prompt.content));
