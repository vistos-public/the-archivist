import 'dotenv/config';
import express from 'express';
import { InteractionType, InteractionResponseType } from 'discord-interactions';
import { Archivist, getRandomEmoji, capitalize } from './archivist.js';
import { InjectionNeededResponse } from './response.js';
import { defaultPrompts } from './prompt.js';

console.log(defaultPrompts);

const app = express();
const archivist = new Archivist();
var injectedPrompts = {};


const PORT = process.env.PORT || 3000;
const ERROR_MSG = 'I\'m sorry, it appears an error has occurred';

app.use(express.json({ verify: archivist.discord.verifyRequest(process.env.PUBLIC_KEY) }));

app.post('/interactions', async function (req, res) {

	const { type, data, token, member, channel_id, application_id, guild_id } = req.body;

	if (type === InteractionType.PING) {
		return res.send({ type: InteractionResponseType.PONG });
	}

	else if (type === InteractionType.APPLICATION_COMMAND) {
		const { name } = data;
		const { display_name, username } = member.user;

		const user = display_name ? capitalize(display_name) : username;

		if (name === 'test-archivist') {
			return res.send({
				type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
				data: {
					content: `Greetings, ${user} ${getRandomEmoji()}`
				}
			});
		}
		else if (name === 'summon') {
			if (guild_id && !injectedPrompts[guild_id]) {
				return res.send(new InjectionNeededResponse());
			}

			console.log(`guild ${guild_id} channel ${channel_id}: summoning`);

			const result = res.send({
				type: InteractionResponseType.DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE
			});

			const prompts = guild_id ? injectedPrompts[guild_id] : defaultPrompts;

			console.log(`guild ${guild_id} channel ${channel_id}: prompts retrieved`);

			const context = await archivist.getChannelContext(channel_id);

			console.log(`guild ${guild_id} channel ${channel_id}: context retrieved`);

			const fullContext = prompts.concat(context);
			const { message } = await archivist.openai.getCompletion(fullContext);

			console.log(`guild ${guild_id} channel ${channel_id}: completion retrieved`);

			const content = message ? message.content : ERROR_MSG;

			await archivist.discord.updateFollowUp(application_id, token, {
				content: content
			});

			return result;
		}
		else if (name === 'inject') {

			console.log(`guild ${guild_id} channel ${channel_id}: injecting`);

			const newPrompts = await archivist.getChannelPromptsForInjection(channel_id);
			const promptCount = newPrompts.length;
			const defaultPromptCount = defaultPrompts.length;

			injectedPrompts[guild_id] = defaultPrompts.concat(newPrompts);

			console.log(`guild ${guild_id} channel ${channel_id}: injecting ${promptCount} new prompts, ${defaultPromptCount} default`);

			return res.json({
				type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
				data: { content: promptCount + ' prompts injected' }
			});
		}
		else if (name === 'reinforce') {

			console.log(`guild ${guild_id} channel ${channel_id}: reinforcing`);

			const newPrompts = await archivist.getChannelPromptsForReinforcement(channel_id);
			const promptCount = newPrompts.length;
			const existingPrompts = injectedPrompts[guild_id];
			const existingPromptCount = existingPrompts ? existingPrompts.length : 0;
			injectedPrompts[guild_id] = existingPromptCount > 0 ? existingPrompts.concat(newPrompts) : newPrompts;

			console.log(`guild ${guild_id} channel ${channel_id}: reinforcing ${existingPromptCount} prompts with ${promptCount} new prompts`);

			return res.json({
				type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
				data: { content: promptCount + ' prompts used for reinforcement' }
			});
		}
		else {
			return res.json({
				type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
				data: { content: 'command unrecognized' }
			});
		}
	}
});

app.listen(PORT, () => {
	console.log('Listening on port', PORT);
});
