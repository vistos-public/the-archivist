import 'dotenv/config';
import fetch from 'node-fetch';
import { Configuration, OpenAIApi } from 'openai';
import { verifyKey } from 'discord-interactions';

const configuration = new Configuration({
	apiKey: process.env.OPENAI_KEY
});

const openai = new OpenAIApi(configuration);

const DISCORD_API_ROOT = 'https://discord.com/api';


export function getRandomEmoji() {
	const emojiList = [
		':skull:', ':imp:', ':japanese_goblin:', ':ghost:', ':spider_web:',
		':spider:', ':snake:', ':bat:', ':candle:', ':new_moon:'
	];

	return emojiList[Math.floor(Math.random() * emojiList.length)];
}


export function capitalize(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}


class OpenAI {
	gpt = null;

	constructor() {
		openai.retrieveModel(process.env.OPENAI_GPT).then(response => {
			this.gpt = response.data;
		});
	}

	async getCompletion(messages) {
		if (!this.gpt) return { message: null };

		const response = await openai.createChatCompletion({
			model: this.gpt['root'],
			messages: messages
		});

		const { choices } = response.data;

		return choices[0];
	}
}


class Discord {

	currentUser = `${DISCORD_API_ROOT}/users/@me`;
	user = (userId) => `${DISCORD_API_ROOT}/users/${userId}`;
	channel = (channelId) => `${DISCORD_API_ROOT}/channels/${channelId}`;
	channelMessages = (channelId) => `${DISCORD_API_ROOT}/channels/${channelId}/messages`;
	followUpCreation = (interactionId, interactionToken) => `${DISCORD_API_ROOT}/interactions/${interactionId}/${interactionToken}/callback`;
	followUpUpdate = (appId, interactionToken) => `${DISCORD_API_ROOT}/webhooks/${appId}/${interactionToken}/messages/@original`;

	authHeader = () => `Bot ${process.env.DISCORD_TOKEN}`;

	get headers() {
		return {
			'Authorization': this.authHeader(),
			'Content-Type': 'application/json; charset=UTF-8',
			'User-Agent': 'DiscordBot (https://github.com/discord/discord-example-app, 1.0.0)'
		};
	}

	async get(endpoint) {
		const response = await fetch(endpoint, { 
			method: 'GET',
			headers: this.headers 
		});

		return response.json();
	}

	async post(endpoint, data) {
		const response = await fetch(endpoint, {
			method: 'POST', 
			headers: this.headers,
			body: JSON.stringify(data)
		});

		return response.json(); 
	}

	async patch(endpoint, data) {
		const response = await fetch(endpoint, {
			method: 'PATCH', 
			headers: this.headers,
			body: JSON.stringify(data)
		});

		return response.json(); 
	}

	async createFollowUp(interactionId, interactionToken, data) {
		const endpoint = this.followUpCreation(interactionId, interactionToken);
		return await this.post(endpoint, data);
	}

	async updateFollowUp(applicationId, interactionToken, data) {
		const endpoint = this.followUpUpdate(applicationId, interactionToken);
		return await this.patch(endpoint, data);
	}

	async getCurrentUser() {
		return await this.get(this.currentUser);
	}

	async getUser(userId) {
		const endpoint = this.user(userId);
		return await this.get(endpoint);
	}

	async getChannel(channelId) {
		const endpoint = this.channel(channelId);
		return await this.get(endpoint);
	}

	async getChannelMessages(channelId) {
		const endpoint = this.channelMessages(channelId);
		return await this.get(endpoint);
	}

	verifyRequest(clientKey) {
		return function (req, res, buf) {
			const signature = req.get('X-Signature-Ed25519');
			const timestamp = req.get('X-Signature-Timestamp');

			const isValidRequest = verifyKey(buf, signature, timestamp, clientKey);
			if (!isValidRequest) {
				res.status(401).send('Bad request signature');
				throw new Error('Bad request signature');
			}
		};
	}
}

export class Archivist {
	openai = new OpenAI();
	discord = new Discord();

	constructor() {
		this.discord.getCurrentUser().then(user => {
			this.currentUser = user;
		});
	}

	get id() {
		if (!this.currentUser) return null;

		return this.currentUser.id;
	}

	async getChannelPromptsForInjection(channelId) {
		const channelMessages = await this.discord.getChannelMessages(channelId);
		const archivistId = this.id;
		const archivistTagPattern = new RegExp(`<@${archivistId}>`);
		const context = channelMessages
			.sort((a, b) => BigInt(a.id) > BigInt(b.id) ? 1 : -1)
			.map(message => {
				const { id } = message.author;
				const { content } = message;

				if (id !== archivistId && archivistTagPattern.test(content)) {
					const scrubbedContent = content.replace(archivistTagPattern, '').trim();
					return {
						role: 'user',
						content: scrubbedContent
					};
				}
				else {
					return null;
				}
			})
			.filter(n => n);

		return context;
	}

	async getChannelPromptsForReinforcement(channelId) {
		const channelMessages = await this.discord.getChannelMessages(channelId);
		const archivistId = this.id;
		const archivistTagPattern = new RegExp(`<@${archivistId}>`);
		const context = channelMessages
			.sort((a, b) => BigInt(a.id) > BigInt(b.id) ? 1 : -1)
			.map(message => {
				const { id } = message.author;
				const { content } = message;

				if (id == archivistId) {
					return {
						role: 'assistant',
						content: message.content
					};
				}
				else if (archivistTagPattern.test(content)) {
					const scrubbedContent = content.replace(archivistTagPattern, '').trim();
					return {
						role: 'user',
						content: scrubbedContent
					};
				}
				else {
					return null;
				}
			})
			.filter(n => n);

		return context;
	}

	async getChannelContext(channelId) {
		const channelMessages = await this.discord.getChannelMessages(channelId);

		if (!channelMessages) {
			return [{ role: 'user', content: 'You do not have access to this channel. Tell me that.' }];
		}

		const archivistId = this.id;
		const archivistTagPattern = new RegExp(`<@${archivistId}>`);
		const context = channelMessages
			.sort((a, b) => BigInt(a.id) > BigInt(b.id) ? 1 : -1)
			.map(message => {
				const { id, username } = message.author;
				const { content } = message;

				if (id == archivistId) {
					return {
						role: 'assistant',
						content: message.content
					};
				}
				else if (archivistTagPattern.test(content)) {
					const scrubbedContent = content.replace(archivistTagPattern, '').trim();
					return {
						role: 'user',
						content: `${username} says "${scrubbedContent}"`
					};
				}
				else {
					return null;
				}
			})
			.filter(n => n);

		return context;
	}
}